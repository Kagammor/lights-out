$(document).ready(function() {
    var lightsout = {
        'board': $('#lightsout'),
        'size': 5,
        'game': [],
        'clicks': 0,
        'clock': 0,
        'time': 0,
        'toggle': function(tile) {
            lightsout.game[tile] ^= 1;
            if(tile % lightsout.size !== 0) {
                lightsout.game[tile - 1] ^= 1;
            }
            if(tile % lightsout.size !== lightsout.size - 1) {
                lightsout.game[tile + 1] ^= 1;
            }
            if(tile - lightsout.size >= 0) {
                lightsout.game[tile - lightsout.size] ^= 1;
            }
            if(tile + lightsout.size < Math.pow(lightsout.size, 2)) {
                lightsout.game[tile + lightsout.size] ^= 1;
            }

            lightsout.render();
        },
        'render': function() {
            lightsout.board.empty();

            var tilesize = lightsout.board.width() / lightsout.size - 10;

            for(var tile in lightsout.game) {
                var newtile = $('<li class="tile ' + (lightsout.game[tile] === 1 ? 'tile-on' : '') + '" id="' + tile + '" style="width: ' + tilesize + 'px; height: ' + tilesize + 'px;">');
                lightsout.board.append(newtile);
            }

            $('#clicks').text(lightsout.clicks + ' clicks');

            $('.tile').on('click', function() {
                lightsout.clicks+=1;
                lightsout.toggle(parseInt(this.id));

                // Game is won
                if(lightsout.game.indexOf(1) === -1 || lightsout.game.indexOf(0) === -1) {
                    window.clearInterval(lightsout.clock);

                    $('.indicator').addClass('won');
                }
            });
        },
        'random': function() {
            lightsout.game = [];

            for(var i=0;i<Math.pow(lightsout.size, 2);i+=1) {
                lightsout.game.push(0);
            }

            for(var i=0;i<Math.pow(lightsout.size, 2);i+=1) {
                if(Math.random() <= 0.5) {
                    lightsout.toggle(i);
                }
            }

            $('.indicator').removeClass('won');
            lightsout.clicks = 0;

            lightsout.time = 0;
            window.clearInterval(lightsout.clock);
            $('#time').text('0:00');

            lightsout.clock = setInterval(function() {
                lightsout.time+=1;

                $('#time').text(Math.floor(lightsout.time / 60) + ':' + (lightsout.time % 60 < 10 ? '0' + lightsout.time % 60 : lightsout.time % 60));
            }, 1000);

            lightsout.render();
        }
    }

    lightsout.random();

    $('#reset').on('click', function() {
        lightsout.random();
    });
});
